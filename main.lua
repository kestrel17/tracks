baton = require 'baton-1.0.2'
home = os.getenv('HOME')

local s = {name = 'tmx test'}

function s:load()
tmxloader = require 'LOVEly-tiles/tmxloader'
tmxsaver = require('LOVEly-tiles/tmxsaver')

drawlist = tmxload 'maps/whistlecave.tmx'

scroll_speed = 500
x,y = 0,0
end

function s:draw()
	local x,y = math.floor(x), math.floor(y)
	drawlist:draw(x,y)
	
	love.graphics.setColor(100,100,100)
end

return s
