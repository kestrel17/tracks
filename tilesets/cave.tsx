<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="cave" tilewidth="16" tileheight="16" tilecount="1000" columns="40">
 <image source="../Downloads/gfx/cave.png" width="640" height="400"/>
 <tile id="285">
  <animation>
   <frame tileid="285" duration="100"/>
   <frame tileid="286" duration="100"/>
   <frame tileid="287" duration="100"/>
   <frame tileid="288" duration="100"/>
   <frame tileid="325" duration="100"/>
   <frame tileid="326" duration="100"/>
   <frame tileid="327" duration="100"/>
   <frame tileid="328" duration="100"/>
  </animation>
 </tile>
 <tile id="367">
  <animation>
   <frame tileid="367" duration="100"/>
   <frame tileid="368" duration="100"/>
   <frame tileid="369" duration="100"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="Pool" type="edge" tile="-1">
   <wangcolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="Corners" color="#00ff00" tile="-1" probability="1"/>
   <wangtile tileid="280" wangid="0,0,1,0,1,0,0,0"/>
   <wangtile tileid="281" wangid="0,0,1,0,0,0,1,0"/>
   <wangtile tileid="282" wangid="0,0,0,0,1,0,1,0"/>
   <wangtile tileid="283" wangid="0,0,2,0,2,0,0,0"/>
   <wangtile tileid="284" wangid="0,0,0,0,2,0,2,0"/>
   <wangtile tileid="320" wangid="1,0,0,0,1,0,0,0"/>
   <wangtile tileid="322" wangid="1,0,0,0,1,0,0,0"/>
   <wangtile tileid="323" wangid="2,0,2,0,0,0,0,0"/>
   <wangtile tileid="324" wangid="2,0,0,0,0,0,2,0"/>
   <wangtile tileid="360" wangid="1,0,1,0,0,0,0,0"/>
   <wangtile tileid="361" wangid="0,0,1,0,0,0,1,0"/>
   <wangtile tileid="362" wangid="1,0,0,0,0,0,1,0"/>
  </wangset>
  <wangset name="Lake" type="corner" tile="-1">
   <wangcolor name="Rock" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="Water" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangtile tileid="0" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="280" wangid="0,0,0,2,0,0,0,0"/>
   <wangtile tileid="281" wangid="0,0,0,2,0,2,0,0"/>
   <wangtile tileid="282" wangid="0,0,0,0,0,2,0,0"/>
   <wangtile tileid="283" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="284" wangid="0,0,0,0,0,1,0,0"/>
   <wangtile tileid="285" wangid="0,2,0,2,0,2,0,2"/>
   <wangtile tileid="320" wangid="0,2,0,2,0,0,0,0"/>
   <wangtile tileid="322" wangid="0,0,0,0,0,2,0,2"/>
   <wangtile tileid="323" wangid="0,1,0,0,0,0,0,0"/>
   <wangtile tileid="324" wangid="0,0,0,0,0,0,0,1"/>
   <wangtile tileid="360" wangid="0,2,0,0,0,0,0,0"/>
   <wangtile tileid="361" wangid="0,2,0,0,0,0,0,2"/>
   <wangtile tileid="362" wangid="0,0,0,0,0,0,0,2"/>
   <wangtile tileid="363" wangid="0,0,0,1,0,0,0,1"/>
   <wangtile tileid="364" wangid="0,1,0,0,0,1,0,0"/>
  </wangset>
  <wangset name="Pit" type="corner" tile="-1">
   <wangcolor name="Ground" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="Void" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="Ledge" color="#0000ff" tile="-1" probability="1"/>
   <wangcolor name="Water" color="#ff7700" tile="-1" probability="1"/>
   <wangtile tileid="121" wangid="0,1,0,3,0,1,0,1"/>
   <wangtile tileid="122" wangid="0,1,0,3,0,3,0,1"/>
   <wangtile tileid="123" wangid="0,1,0,1,0,3,0,1"/>
   <wangtile tileid="127" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="161" wangid="0,3,0,3,0,1,0,1"/>
   <wangtile tileid="162" wangid="0,3,0,3,0,3,0,3"/>
   <wangtile tileid="163" wangid="0,1,0,1,0,3,0,3"/>
   <wangtile tileid="171" wangid="0,1,0,2,0,1,0,1"/>
   <wangtile tileid="172" wangid="0,1,0,2,0,2,0,1"/>
   <wangtile tileid="173" wangid="0,1,0,1,0,2,0,1"/>
   <wangtile tileid="174" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="175" wangid="0,0,0,0,0,1,0,0"/>
   <wangtile tileid="201" wangid="0,3,0,1,0,1,0,1"/>
   <wangtile tileid="202" wangid="0,3,0,1,0,1,0,3"/>
   <wangtile tileid="203" wangid="0,1,0,1,0,1,0,3"/>
   <wangtile tileid="211" wangid="0,2,0,2,0,1,0,1"/>
   <wangtile tileid="213" wangid="0,1,0,1,0,2,0,2"/>
   <wangtile tileid="214" wangid="0,1,0,0,0,0,0,0"/>
   <wangtile tileid="215" wangid="0,0,0,0,0,0,0,1"/>
   <wangtile tileid="251" wangid="0,2,0,1,0,1,0,1"/>
   <wangtile tileid="252" wangid="0,2,0,1,0,1,0,2"/>
   <wangtile tileid="253" wangid="0,1,0,1,0,1,0,2"/>
   <wangtile tileid="254" wangid="0,0,0,1,0,0,0,1"/>
   <wangtile tileid="255" wangid="0,1,0,0,0,1,0,0"/>
   <wangtile tileid="280" wangid="0,0,0,4,0,0,0,0"/>
   <wangtile tileid="281" wangid="0,0,0,4,0,4,0,0"/>
   <wangtile tileid="282" wangid="0,0,0,0,0,4,0,0"/>
   <wangtile tileid="283" wangid="0,4,0,0,0,4,0,4"/>
   <wangtile tileid="284" wangid="0,4,0,4,0,0,0,4"/>
   <wangtile tileid="320" wangid="0,4,0,4,0,0,0,0"/>
   <wangtile tileid="321" wangid="0,4,0,4,0,4,0,4"/>
   <wangtile tileid="322" wangid="0,0,0,0,0,4,0,4"/>
   <wangtile tileid="323" wangid="0,0,0,4,0,4,0,4"/>
   <wangtile tileid="324" wangid="0,4,0,4,0,4,0,0"/>
   <wangtile tileid="360" wangid="0,4,0,0,0,0,0,0"/>
   <wangtile tileid="361" wangid="0,4,0,0,0,0,0,4"/>
   <wangtile tileid="362" wangid="0,0,0,0,0,0,0,4"/>
   <wangtile tileid="363" wangid="0,4,0,0,0,4,0,0"/>
   <wangtile tileid="364" wangid="0,0,0,4,0,0,0,4"/>
  </wangset>
 </wangsets>
</tileset>
