#!/bin/sh

LUAROCKS_SYSCONFDIR='/etc/luarocks' exec '/usr/bin/lua5.4' -e 'package.path="/home/spirit/.luarocks/share/lua/5.4/?.lua;/home/spirit/.luarocks/share/lua/5.4/?/init.lua;/usr/share/lua/5.4/?.lua;/usr/share/lua/5.4/?/init.lua;"..package.path;package.cpath="/home/spirit/.luarocks/lib/lua/5.4/?.so;/usr/lib/lua/5.4/?.so;"..package.cpath' $([ "$*" ] || echo -i) "$@"
